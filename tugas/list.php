<?php include ('database.php'); 
?>
<!DOCTYPE html>
<html>
<head>  
<style>
div {
  background: url('image/bg.png') no-repeat center center fixed;
  background-size: cover;
  overflow: hidden;
  width: 100%;
    height: 100%;
}
</style>
    <title>List User</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../theme/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="../theme/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="../theme/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="../theme/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../theme/dist/css/skins/_all-skins.min.css">
</head>

<body>

<div class="box">
            <div class="box-header">
</br>
<form action="list.php" method="get">
	<label><p style="color:aliceblue">Pencarian : </p></label>
	<input type="text" name="cari">
	<input type="submit" class='btn btn-info' value="Cari">
</form>
  </style>
  <?php 
if(isset($_GET['cari'])){
	$cari = $_GET['cari'];
	echo "<b>Hasil pencarian : ".$cari."</b>";
}
?>


              <h3 style="color:White;" class="box-title; text-center">List User</h3>
            </div>
        </br>
        <?php
            if(isset($_GET['status'])){
    if($_GET['status']=="sukses-hapus"){
        echo "<div class='alert alert-danger'> Data telah dihapus </div>";
    }
    if($_GET['status']=="sukses"){
        echo "<div class='alert alert-warning'> Data telah diperbarui </div>";
    }
    if($_GET['status']=="simpan"){
      echo "<div class='alert alert-success'> Admin telah ditambah </div>";
  }
}
?>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <table id="myTable" class="table table-striped">
                <tbody><tr align="center" style="color:White;">
                  <th style="width: 10px">No</th>
                  <th>Username</th>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>Peran</th>
                  <th style="width: 30px" colspan="2">Aksi</th>
                </tr>
                <?php
                if(isset($_GET['cari'])){
                 $cari = $_GET['cari'];
                 $query = mysqli_query($database, "select * from jabatan where nama like '%".$cari."%' or username like '%".$cari."%'");				
                  }else{
                  $query = mysqli_query($database, "select * from jabatan");		
                  }
                  $no = 1;
        $cek = mysqli_num_rows($query);
        while($row = mysqli_fetch_array($query)){
            echo "<tr style='color:White'>";
            echo "<td>".$no++."</td>";
            echo "<td>".$row['username']."</td>";
            echo "<td>".$row['nama']."</td>";
            echo "<td>".$row['email']."</td>";
            if ($row['level']==1){
              echo "<td>Admin</td>";
            } else echo "<td>User</td>";
            echo "<td>"."<a href='edit_data.php?id=".$row['id']."' class='btn btn-warning' role='button'>Edit</a></td>";
            echo "<td>"."<a href='hapus.php?id=".$row['id']."' class='btn btn-danger' role='button' >Delete</a></td>";
            echo "</tr>";
        } 
        ?>
              </tbody></table>
            </div>
     </tbody>
    </table>
    <h5 style="color:White; border:2px solid beige;" align="center"> Total User : <?php echo $cek ?></h5>  
    <a href = "admin/register.php" class="btn btn-primary" role="button">ADD ADMIN</a>
    <a href = "admin/index.php" class="btn btn-success" role="button">KEMBALI</a>
    </div>

</body>
</html>