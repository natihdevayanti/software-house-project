<!DOCTYPE html>
<html>
<head>
    <link rel= "stylesheet" href="css/bootstrap.min.css">
    <title>Tugas</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../theme/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="../theme/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="../theme/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="../theme/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../theme/dist/css/skins/_all-skins.min.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
div {
  background: url('image/bg.png') no-repeat center center fixed;
  background-size: cover;
  height: 100%;
  overflow: hidden;
}
</style>
</head>
<body> 
    <!-- cek pesan notifikasi -->
    <div class="container">
</br>
    <h2>LOGIN</h2>
    <br/>
    <?php
        //to avoid miss character input
        function input ($data){
            $data = trim($data);
            $data = stripslashes ($data);
            $data = htmlspecialchars($data);
            return $data;
        }
        if(isset($_GET['pesan'])){
            if($_GET['pesan']=="gagal"){
                echo "<div class='alert alert-danger'
                <strong>Error!</strong> Username dan password salah. </div>";
            }
            else if ($_GET['pesan'] == "logout"){
                echo "<div class='alert alert-info'
                <strong>Info!</strong> Anda telah berhasil logout. </div>";
            }
            else if ($_GET['pesan']== "belum_login"){
                echo "<div class='alert alert-success'
                <strong> Sukses! </strong> Berhasil menyimpan data. Silakan melakukan login. </div>";
            }

            }
            else {
                echo "<div class='alert alert-info'
                <strong>Info!</strong> Silakan login terlebih dahulu. </div>";
            }
    
        ?>
        <br/>
        <form method="post" action ="cek_login.php" style=color:aliceblue;>
        <div class="form-group">
            <label>Username</label>
            <td>:</td>
            <input type="text" class="form-control" name="username" placeholder="Masukkan Username">
        </div>
        <div class="form-group">
            <label>Password</label>
            <td>:</td>
            <input type="password" class="form-control" name="password" placeholder="Masukkan Password">
        </div>
        <div class="form-group">
            <input type="submit"  class="btn btn-primary"  value="LOGIN">
            <a href = "index.php" class="btn btn-success" role="button">KEMBALI</a>
        </div>
    </form>
    </body>
</html>