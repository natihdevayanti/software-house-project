<?php
    include ('database.php');

    //take this from URL
    $id= $_GET['id'];

    $query = mysqli_query($database, 'select * from jabatan where id= '.$id);
    $akun = mysqli_fetch_assoc($query);
    if(mysqli_num_rows($query)<1){
        header('Location:index.php');
    }
?>
<!DOCTYPE html>
<html>
<head>
    <!-- Load file CSS Bootstrap offline -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../theme/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="../theme/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="../theme/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="../theme/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../theme/dist/css/skins/_all-skins.min.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
div {
  background: url('../image/background.png') no-repeat center center fixed;
  background-size: cover;
  height: 100%;
  overflow: hidden;
}
</style>
</head>
<body>
<div class="container">
</br>
<h2 style="color:aliceblue">EDIT PROFILE ADMIN</h2>
</br>
    <form action="perbarui.php" method="post">
        <input type="hidden" name="id" value="<?php echo $akun['id']?>">
        <div class="form-group">
            <label>Username:</label>
            <input type="text" name="username" class="form-control" placeholder="Masukkan Username" value="<?php echo $akun['username'] ?>" />
        </div>
	<div class="form-group">
            <label>Nama:</label>
            <input type="text" name="nama" class="form-control" placeholder="Masukkan Nama" value="<?php echo $akun['nama'] ?>"/>
        </div>
	<div class="form-group">
            <label>Email:</label>
            <input type="email" name="email" class="form-control" placeholder="Masukkan Email" value="<?php echo $akun['email'] ?>" />
</div>
<div class="form-group">
            <label>Password:</label>
            <input type="password" name="password" class="form-control" placeholder="Masukkan Password" value="<?php echo $akun[md5('password')] ?>" />
</div>
        <input type="submit" name="submit" class="btn btn-primary" value= "PERBARUI"/>
        <a href = "index.php" class="btn btn-success" role="button">KEMBALI</a>
    </form>
</div>

</body>
</html>