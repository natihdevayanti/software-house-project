<?php
        include ('database.php');
        $sql=mysqli_query($database, "select * from jabatan");
        $cek = mysqli_num_rows($sql);
        session_start();
        if($_SESSION['status']!="login"){
            header("location:../login.php?pesan=belum_login");
        }
        $level=$_SESSION["level"];
        if ($level!=1) {
            echo "Anda tidak memiliki akses pada halaman admin";
            exit;
        }
        $id=$_SESSION["id"];
        $username=$_SESSION["username"];
        $nama=$_SESSION["nama"];
        $email=$_SESSION["email"];
        ?>
<!DOCTYPE html>
<html>
    <head>
        <title>Tugas</title>
        <link rel="stylesheet" href="../css/bootstrap.min.css">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../theme/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="../theme/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="../theme/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="../theme/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../theme/dist/css/skins/_all-skins.min.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <style>
div {
  background: url('../image/bg.png') no-repeat center center fixed;
  background-size: cover;
  height: 100%;
  overflow: hidden;
}
</style>

    </head>

    <body>

        <!-- checking if the admin has login/not -->
        <div class= "jumbotron text-center" style=color:aliceblue;>
        <h1>Halaman Admin</h1>
        <?php
         $query = mysqli_query($database, "select * from jabatan where id=$id");
         $row = mysqli_fetch_array($query);
         echo "<h3> Selamat datang, ".$row['nama']." ! </h3>";
     echo "<br/>";
      if(isset($_GET['status'])){
        if($_GET['status']=="sukses"){
            echo "<a class='alert alert-success'> Data telah diperbarui </a>";
        } else  echo "<a class='alert alert-warning'> Data gagal diperbarui </a>";
    }
    echo "<br/>";
    echo "<br/>";
        echo "<span style='background-color: #4682B4' class='dua'>PROFILE</span>";
        echo "<br/>";
        echo "<span style='background-color: #2F4F4F' class='dua'>  Username : </span>";
        echo "<span  class='dua'>  "  .$row['username']. "</span>";
        echo "<br/>";
        echo "<span style='background-color: #008080' class='dua'>  Email : </span>";
        echo "<span class='dua'> "  .$row['email']. "</span>";
        echo "<br/>";
        echo "<br/>";
        echo "<br/>";
        echo "<br/>";
        echo "<td>"."<a href='edit_profile.php?id=".$row['id']."' class='btn btn-info' role='button'>Edit Profile</a></td>";

        ?>
        <style>
            .dua{
                font-size: 20px;
            }
        </style>
</br>
</br>

        <br/>
        <div class="col-md-3 col-sm-6 col-xs-12">
          
            <span class="info-box-icon bg-green"><i class="fa fa-user"></i></span>
            <p class="info-box-content">
              <span class="info-box-text">Total User</span>
              <span class="info-box-number"><?php echo $cek ?></span>
              <a href = "../list.php" class="btn btn-warning" role="button">Lihat User</a>
  </p>
            <!-- /.info-box-content -->
          <!-- /.info-box -->
        </div>
        <br/>
        <!-- ./col -->
        <a href = "logout.php" class="btn btn-danger" role="button" style=float:right;>LOGOUT</a>

        
</div>
    </body>
</html>