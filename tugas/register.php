<!DOCTYPE html>
<html>
<head>
    <!-- Load file CSS Bootstrap offline -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../theme/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="../theme/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="../theme/bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="../theme/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../theme/dist/css/skins/_all-skins.min.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<style>
div {
  background: url('image/bg.png') no-repeat center center fixed;
  background-size: cover;
  height: 100%;
  overflow: hidden;
}
</style>
<body>
<div class="container">
</br>
<h2>REGISTRATION</h2>
</br>
    <form action="simpan.php" method="post" style=color:aliceblue;>
        <div class="form-group">
            <label>Username:</label>
            <input type="text" name="username" class="form-control" placeholder="Masukkan Username" />
        </div>
	<div class="form-group">
            <label>Nama:</label>
            <input type="text" name="nama" class="form-control" placeholder="Masukkan Nama" />
        </div>
	<div class="form-group">
            <label>Email:</label>
            <input type="email" name="email" class="form-control" placeholder="Masukkan Email" />
        </div>
	<div class="form-group">
            <label>Password:</label>
            <input type="password" name="password" class="form-control" placeholder="Masukkan Password" />
        </div>

        <p>Sudah punya akun? <a href="login.php">Login disini</a></p>
       
        <input type="submit" name="submit" class="btn btn-primary" value= "REGISTER"/>
        <a href = "index.php" class="btn btn-success" role="button">KEMBALI</a>
    </form>
</div>

</body>
</html>